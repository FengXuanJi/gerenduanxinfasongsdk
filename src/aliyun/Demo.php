<?php

use msms\Sms;
class Demo
{
    public function index(){
        $obj = Sms::make('aliyun',[ "accessKeyId" => '', "accessKeySecret" => '']);
        //添加模板
        $res = $obj->AddSmsTemplate([
            'templateType'=>0,
            'templateName'=>'用户登录获取短信验证码',
            'templateContent'=>'您正在申请手机登录房倌儿，验证码为：${code}，5分钟内有效！',
            'remark'=>'当前模板用于用户登录获取短信验证码',
        ]);
        //查询模板状态
        $res = $obj->QuerySmsTemplate('SMS_220621621');
        //删除模板
        $res = $obj->DeleteSmsTemplate('SMS_220636579');
        //吸怪模板
        $res = $obj->ModifySmsTemplate([
            'templateCode'=>'SMS_220626524',
            'templateType'=>0,
            'templateName'=>'用户登录获取短信验证码',
            'templateContent'=>'您正在申请手机登录房倌儿，验证码为：${code}，5分钟内有效！',
            'remark'=>'用于用户登录获取短信验证码',
        ]);
        //国内发送短信
        $res = $obj->SendSms(['code'=>'5201314'],['182****8197'],'SMS_220621621','客服');
        //国际发送短信
        $res = $obj->SendMessageToGlobe(['code'=>'520134'],['182****8197'],'SMS_220621621','客服');
        //群发消息
        $res = $obj->SendBatchSms([['code'=>'520134'],['code'=>'5201314']],['182****8197'],'SMS_220621621',['客服']);
        if($res===false){
            echo $obj->getMessage();exit;
        }
        echo '<pre>';
        print_r($res);
        exit;
    }
}