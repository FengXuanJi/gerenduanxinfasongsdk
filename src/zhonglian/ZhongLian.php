<?php
namespace msms\zhonglian;
use msms\Curl;
use msms\SmsConfig;

class ZhongLian extends SmsConfig{
    protected $url = 'http://121.201.57.213/smsJson.aspx';
    public function __construct($config=[])
    {
        foreach ($config as $key=>$value){
            $this->config[$key] = $value;
        }
        $this->curl = new Curl();
    }
    public function SendSms(array $message = [], array $phone = [], string $TemplateCode = '', string $SignName = '')
    {
        $url = $this->url;
        $data = [
            'action'=>'send',
            'userid'=>'',
            'account'=>$this->config['account'],
            'password'=>$this->config['password'],
            'mobile'=>implode(',',$phone),
            'content'=>implode(',',$message),
            'sendTime'=>'',
        ];
        $bool = $this->curl->http_requests($url,$data);
        try {
            $bool = json_decode($bool,true);
        }catch (\Exception $e){
            $this->message = $e->getMessage();
            return $bool;
        }
        return $bool;
    }
    public function SendMessageToGlobe(array $message = [], array $phone = [], string $TemplateCode = '', string $SignName = '')
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement SendMessageToGlobe() method.
    }
    public function SendBatchSms($TemplateParamJson = [], $PhoneNumberJson = [], $TemplateCode = '', $SignNameJson = [])
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement SendBatchSms() method.
    }
    public function AddSmsTemplate($data = [])
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement AddSmsTemplate() method.
    }
    public function QuerySmsTemplate($data)
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement QuerySmsTemplate() method.
    }
    public function ModifySmsTemplate($data = [])
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement ModifySmsTemplate() method.
    }
    public function DeleteSmsTemplate($TemplateCode)
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement DeleteSmsTemplate() method.
    }
    public static function send($mobile='13980159277', $content='您好！您的推广收益时间已到期，请分享商品获取推广收益时间。【西藏宇丰印务有限公司】')
    {
        $account = 'scmtyfyw';
        $password = 'mt192939';
        $url = 'http://121.201.57.213/smsJson.aspx';
        $data = [
            'action'=>'send',
            'userid'=>'',
            'account'=>$account,
            'password'=>$password,
            'mobile'=>'13980159277',
            'content'=>$content,
            'sendTime'=>'',
        ];
        $res = http_request($url, $data, 'POST');
        if (!$res){
            \think\facade\Log::write('发送短信失败'.$mobile);
        }
        $result = json_decode($res, true);
        if($result['returnstatus'] == 'Success'){
            $resp = array('status' => 1, 'msg' => '发送成功');
        }else{
            $resp = array('status' => 0, 'msg' => '发送失败');
        }
        return $resp;
    }
}