<?php
namespace msms;
abstract class SmsConfig
{
    protected $config = [];

    protected $message = '';
    public function getMessage(){
        return $this->message;
    }
    /**设置配置项
     * @param array $config
     * @return $this
     */
    public function setConfig(array $config=[]){
        $this->config = $config;
        return $this;
    }

    /**发送国内短信
     * @param array模板短信需要填充 $message
     * @param array接收用户 $phone
     * @param string模板code $TemplateCode
     * @param string签名 $SignName
     * @return mixed
     */
    abstract public function SendSms(array $message=[],array $phone=[],string $TemplateCode='',string $SignName='');

    /**发送国际短信
     * @param array模板短信需要填充 $message
     * @param array接收用户 $phone
     * @param string模板code $TemplateCode
     * @param string签名 $SignName
     * @return mixed
     */
    abstract public function SendMessageToGlobe(array $message=[],array $phone=[],string $TemplateCode='',string $SignName='');

    /**群发消息
     * @param array短信模板变量对应的实际值 $TemplateParamJson
     * @param array接收短信的手机号码 $PhoneNumberJson
     * @param string短信模板CODE $TemplateCode
     * @param array短信签名名称 $SignNameJson
     * @return mixed
     */
    abstract public function SendBatchSms($TemplateParamJson=[],$PhoneNumberJson=[],$TemplateCode='',$SignNameJson=[]);
    /**申请添加签名
     * @param array $data
     * @return mixed
     */
    abstract public function AddSmsTemplate($data=[]);

    /**查询模板审核状态
     * @param array $data
     * @return mixed
     */
    abstract public function QuerySmsTemplate($data);

    /**修改未通过审核的短信模板
     * @param array $data
     * @return mixed
     */
    abstract public function ModifySmsTemplate($data=[]);

    /**删除模板
     * @param string $TemplateCode
     * @return mixed
     */
    abstract public function DeleteSmsTemplate($TemplateCode);

}