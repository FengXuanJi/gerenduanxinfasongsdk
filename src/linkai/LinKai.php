<?php
namespace msms\linkai;
use msms\Curl;
use msms\SmsConfig;

class LinKai extends SmsConfig
{
    protected $curl = '';
    protected $url = "https://sdk3.028lk.com:9988/";
    protected $config = [];
    public function __construct($config=[])
    {
        foreach ($config as $key=>$value){
            $this->config[ucfirst($key)] = $value;
        }
        $this->curl = new Curl();
    }
    protected function http_requests(string $url,array $data = [],array $header= [],$method='POST'){
        $curpost = '';
        foreach ($data as $key=>$value){
            if($key=='Content'||$key=='content'){
                $curpost .= "{$key}=".rawurlencode(mb_convert_encoding($value, "gb2312", "utf-8")).'&';
            }else{
                $curpost .= "{$key}=".$value."&";
            }
        }
        $curpost = $curpost."SendTime=";
        //POST方式请求
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查 -https
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curpost);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        if($error = curl_errno($ch)){
            $this->message = $error;
            return false;
        }
        curl_close($ch);
        return $result;
    }

    public function SendSms(array $message = [], array $phone = [], string $TemplateCode = '', string $SignName = '')
    {
        $url = $this->url.'BatchSend2.aspx';
//        echo '<pre>';
//        print_r($url);
//        exit;
        $data = [
            "CorpID"=>$this->config['CorpID'],
            "Pwd"=>$this->config['Pwd'],
            "Mobile"=>implode(',',$phone),
            "Content"=>implode(',',$message)
        ];
        $bool = $this->http_requests($url,$data);
        return $bool;
    }
    public function SendMessageToGlobe(array $message = [], array $phone = [], string $TemplateCode = '', string $SignName = '')
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement SendMessageToGlobe() method.
    }
    public function SendBatchSms($TemplateParamJson = [], $PhoneNumberJson = [], $TemplateCode = '', $SignNameJson = [])
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement SendBatchSms() method.
    }
    public function AddSmsTemplate($data = [])
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement AddSmsTemplate() method.
    }
    public function QuerySmsTemplate($data)
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement QuerySmsTemplate() method.
    }
    public function ModifySmsTemplate($data = [])
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement ModifySmsTemplate() method.
    }
    public function DeleteSmsTemplate($TemplateCode)
    {
        $this->message = '本通道没有此方法';
        return false;
        // TODO: Implement DeleteSmsTemplate() method.
    }
}