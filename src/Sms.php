<?php
namespace msms;

use msms\aliyun\Alisms;
use msms\linkai\LinKai;
use msms\zhonglian\ZhongLian;

class Sms
{
    /**创建短信类型
     * @param string $type
     * @param array $config
     * @return Alisms
     * @throws \Exception
     */
    public static function make(string $type='aliyun',array $config=[]){
        if(strtolower($type)=='aliyun'){
            return new Alisms($config);
        }elseif ($type=='linkai'){
            return new LinKai($config);
        }elseif ($type=='zhonglian'){
            return new ZhongLian($config);
        }
        throw new \Exception('短信类型不存在');
    }
}