# 个人短信发送sdk

#### 介绍
个人用sdk

#### 软件架构
软件架构说明


#### 安装教程

composer 安装：

composer require maowenke/msms dev-master

git 安装：
git clone https://gitee.com/FengXuanJi/gerenduanxinfasongsdk.git

#### 使用说明

1.  目前只有阿里云发送短信，需要依赖阿里云的sdk

    如果报错ssl不存在请修改阿里云sdk
![img.png](img.png)
    
在这将https修改成http即可
2:已实现凌凯目录：limkai
3:已实现众联目录：zhonglian
备注：每个不同的里面有一个demo，是实际调用的代码

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
